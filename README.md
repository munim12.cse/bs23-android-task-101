# BS23 Android Task - 101



## Getting started

Initial design pattern and other necessary components

1. activity: it contains application activity class.
2. adapter: recycler view adapter.
3. data: it contains all data accessing components.
4. dhhelper: room database particulars.
5. di: Hit module.
6. domain: it contains all types of business manipulating, serving components.
7. fragment: Application fragments, and dialogs etc.
8. helper: Common helper components.
9. network: Network client interfaces and utils.
10. utils: Common utils components.
11. viewmodels: ViewModel components etc.


### Libraries
* [Android Support Library][support-lib]
* [Android Architecture Components][arch]
* [Retrofit][retrofit] for REST api communication
* [Glide][glide] for image loading
* [espresso][espresso] for UI tests
* [mockito][mockito] for mocking in tests
* [Hilt][hilt] dependency injection


### Features
Show the list of GitHub repository in the home page, repository list should be paginated by scrolling 10 new items.
Details page shows repo owner's name, photo, repository's description, last update date time in month-day-year hour:seconds format, each field in 2 digit numbers

# UI
Navigation Fragment Implemented for both Home and Details page.

# Network 
Each Backend API call fetch all query data and caching into room. 

# Paging With Database 
implemented in the [HomeRepositoryImpl] class, demonstrates how to set up
a Repository that will use the local database to page in data for the UI

It uses `Room` to create the `PagingSource` [dao].  The `Pager` creates a stream of
data from the PagingSource to the UI, and more data is paged in as it is consumed.

# Paging3 Architecture

<div align="center">
  <a href="https://gitlab.com/munim12.cse/bs23-android-task-101">
    <img src="paging_architecture.png" alt="Logo" width="550" height="313">
  </a>
</div>


