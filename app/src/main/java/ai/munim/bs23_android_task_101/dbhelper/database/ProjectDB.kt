package ai.munim.bs23_android_task_101.dbhelper.database

import ai.munim.bs23_android_task_101.dbhelper.Converters
import ai.munim.bs23_android_task_101.dbhelper.DbConstants
import ai.munim.bs23_android_task_101.dbhelper.dao.GithubRepoDao
import ai.munim.bs23_android_task_101.dbhelper.entity.GithubRepoEntity

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import javax.inject.Singleton

@Singleton
@Database(
    entities = [GithubRepoEntity::class],
    version = DbConstants.DB_VERSION,
    exportSchema = false
)
//@TypeConverters(Converters::class)
abstract class ProjectDB : RoomDatabase() {

    abstract fun getGithubRepoDao(): GithubRepoDao

}