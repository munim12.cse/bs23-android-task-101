package ai.munim.bs23_android_task_101.dbhelper.dao

import ai.munim.bs23_android_task_101.dbhelper.DbConstants
import ai.munim.bs23_android_task_101.dbhelper.entity.GithubRepoEntity
import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import androidx.paging.PagingSource
import androidx.room.*

@Dao
interface GithubRepoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveGithubInfo(entity: GithubRepoEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveGithubInfo(githubRepositoryList: ArrayList<GithubRepoEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveGithubInfoTest(githubRepositoryList: ArrayList<GithubRepoEntity>)

    @Delete
    suspend fun deleteGithubRepo(entity: GithubRepoEntity)

    @Query("DELETE FROM " + DbConstants.GITHUB_REPO_TABLE)
    suspend fun deleteAllGithubRepo()

    @Update
    suspend fun updateGithubRepo(entity: GithubRepoEntity)

    @Update
    suspend fun updateGithubRepo(entity: ArrayList<GithubRepoEntity>)

    @Query("SELECT * FROM " + DbConstants.GITHUB_REPO_TABLE + " ORDER BY id ASC LIMIT :limit OFFSET :offset")
    suspend fun getPagedGithubRepoList(limit: Int, offset: Int) : List<GithubRepoEntity>

    @Query("SELECT * FROM " + DbConstants.GITHUB_REPO_TABLE+ " ORDER BY stargazersCount DESC LIMIT :limit OFFSET :offset")
    suspend fun getPagedSortedGithubRepoList(limit: Int, offset: Int) : List<GithubRepoEntity>

    @Query("SELECT * FROM " + DbConstants.GITHUB_REPO_TABLE + " ORDER BY id ASC LIMIT :limit OFFSET :offset")
     fun getPagedGithubRepoListTest(limit: Int, offset: Int) : List<GithubRepoEntity>

    @Query("SELECT * FROM " + DbConstants.GITHUB_REPO_TABLE+ " ORDER BY stargazersCount DESC LIMIT :limit OFFSET :offset")
    fun getPagedSortedGithubRepoListTest(limit: Int, offset: Int) : List<GithubRepoEntity>

}