package ai.munim.bs23_android_task_101.dbhelper

object DbConstants {
    const val DB_NAME = "MyApp"
    const val DB_VERSION = 1
    const val GITHUB_REPO_TABLE = "GITHUB_REPO_TABLE"
    const val GITHUB_REPO_TABLE_PRIMARYKEY_ID= "id"
    const val GITHUB_REPO_TABLE_PRIMARYKEY__DATE = "date"
    const val GITHUB_REPO_REMOTE_KEY_TABLE = "remote_keys"
}