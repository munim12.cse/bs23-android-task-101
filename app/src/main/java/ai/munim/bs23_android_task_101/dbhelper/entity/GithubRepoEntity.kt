package ai.munim.bs23_android_task_101.dbhelper.entity

import ai.munim.bs23_android_task_101.dbhelper.DbConstants
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(
    tableName = DbConstants.GITHUB_REPO_TABLE,
)
data class GithubRepoEntity(

    @PrimaryKey(autoGenerate = true)
    val id: Int =0,
    @SerializedName("repo_id")
    val repo_id: Int? = null,
    @SerializedName("stargazersCount")
    val stargazersCount: Int? = null,
    @SerializedName("fullName")
    val fullName: String? = null,
    @SerializedName("name")
    @ColumnInfo(collate = ColumnInfo.NOCASE)
    val name: String? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("createdAt")
    val createdAt: String? = null,
    @SerializedName("watchers")
    val watchers: Int? = null,
    @SerializedName("updatedAt")
    val updatedAt: String? = null,
    @SerializedName("stargazersUrl")
    val stargazersUrl: String? = null,
    @SerializedName("url")
    val url: String? = null,
    @SerializedName("watchersCount")
    val watchersCount: Int? = null,
    @SerializedName("nodeId")
    val nodeId: String? = null,
    @SerializedName("reposUrl")
    val reposUrl: String? = null,
    @SerializedName("avatarUrl")
    val avatarUrl: String? = null,
    @SerializedName("login")
    val login: String? = null,
    @SerializedName("organizationsUrl")
    val organizationsUrl: String? = null
)

