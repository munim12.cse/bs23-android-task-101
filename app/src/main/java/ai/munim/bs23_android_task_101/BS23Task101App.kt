package ai.munim.bs23_android_task_101

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BS23Task101App : Application()