package ai.munim.bs23_android_task_101.network.response

import ai.munim.bs23_android_task_101.network.model.githubDTOs.RepositoryDto
import com.google.gson.annotations.SerializedName

data class GithubRepositoryResponse(

	@field:SerializedName("total_count")
	val totalCount: Int? = null,

	@field:SerializedName("incomplete_results")
	val incompleteResults: Boolean? = null,

	@field:SerializedName("items")
	val items: List<RepositoryDto?>? = null
)


