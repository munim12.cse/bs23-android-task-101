package ai.munim.bs23_android_task_101.network.interfaces

import ai.munim.bs23_android_task_101.network.response.GithubRepositoryResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GithubRetrofitService {


    @GET("search/repositories")
    suspend fun getGithubRepoList(
        @Query("q") q:String): GithubRepositoryResponse

    @GET("search/repositories")
    fun getGithubRepoListWithObserver(@Query("q") q:String): Call<GithubRepositoryResponse>


}