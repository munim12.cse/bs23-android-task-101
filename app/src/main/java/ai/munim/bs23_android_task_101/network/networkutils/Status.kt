package ai.munim.bs23_android_task_101.network.networkutils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING,
    PENDING,
    APPEND
}