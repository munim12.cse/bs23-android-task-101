package ai.munim.bs23_android_task_101.network.model.githubDTOs

import ai.munim.bs23_android_task_101.domain.model.githubRepoModel.GithubRepoModel
import ai.munim.bs23_android_task_101.domain.utils.DomainMapper
import ai.munim.bs23_android_task_101.network.response.GithubRepositoryResponse

class RepositoryDtoMapper : DomainMapper<RepositoryDto, GithubRepoModel> {


    fun repoList(list: List<RepositoryDto>): List<GithubRepoModel> {
        val temp : ArrayList<GithubRepoModel> = ArrayList()
        for (dto in list){
            temp.add(mapToDomainModel(dto))
        }
        return temp
    }

    override fun mapToDomainModel(model: RepositoryDto): GithubRepoModel {
        return GithubRepoModel(model.stargazersCount,
            model.fullName, model.name,model.description,model.createdAt,model.watchers,
            model.updatedAt,model.stargazersUrl, model.url,model.watchersCount,model.nodeId,
            model.owner?.reposUrl, model.owner?.avatarUrl, model.owner?.login,model.id,model.owner?.organizationsUrl)
    }

    override fun mapFromDomainModel(domainModel: GithubRepoModel): RepositoryDto {
        TODO("Not yet implemented")
    }


}