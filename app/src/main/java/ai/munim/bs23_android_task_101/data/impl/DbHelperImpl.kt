package ai.munim.bs23_android_task_101.data.impl

import ai.munim.bs23_android_task_101.data.DbHelper
import ai.munim.bs23_android_task_101.dbhelper.database.ProjectDB
import ai.munim.bs23_android_task_101.dbhelper.entity.GithubRepoEntity
import androidx.paging.PagingSource
import javax.inject.Inject

class DbHelperImpl
@Inject constructor(val projectDB: ProjectDB) : DbHelper {

    override suspend fun saveGithubInfo(entity: GithubRepoEntity) {
        projectDB.getGithubRepoDao().saveGithubInfo(entity)
    }

    override suspend fun saveGithubInfo(entity: ArrayList<GithubRepoEntity>) {
        projectDB.getGithubRepoDao().saveGithubInfo(entity)
    }

    override suspend fun deleteGithubRepo(entity: GithubRepoEntity) {
        projectDB.getGithubRepoDao().deleteGithubRepo(entity)
    }

    override suspend fun deleteAllGithubRepo() {
        projectDB.getGithubRepoDao().deleteAllGithubRepo()
    }

    override suspend fun updateGithubRepo(entity: GithubRepoEntity) {
        projectDB.getGithubRepoDao().updateGithubRepo(entity)
    }

    override suspend fun updateGithubRepo(entity: ArrayList<GithubRepoEntity>) {
        projectDB.getGithubRepoDao().updateGithubRepo(entity)
    }

    override suspend fun getGithubRepoList(limit: Int, offset: Int): List<GithubRepoEntity> {
        return projectDB.getGithubRepoDao().getPagedGithubRepoList(limit,offset)
    }

    override suspend fun getSortedGithubRepoList(limit: Int, offset: Int): List<GithubRepoEntity>  {
        return projectDB.getGithubRepoDao().getPagedSortedGithubRepoList(limit, offset)
    }

    override fun getGithubRepoListTest(limit: Int, offset: Int): List<GithubRepoEntity> {
        return projectDB.getGithubRepoDao().getPagedGithubRepoListTest(limit,offset)
    }

    override fun getSortedGithubRepoListTest(limit: Int, offset: Int): List<GithubRepoEntity> {
        return projectDB.getGithubRepoDao().getPagedSortedGithubRepoListTest(limit, offset)
    }


}