package ai.munim.bs23_android_task_101.data.impl

import ai.munim.bs23_android_task_101.data.SharedPreferenceHelper
import ai.munim.bs23_android_task_101.utils.Constants
import android.content.SharedPreferences
import java.lang.Boolean.FALSE
import javax.inject.Inject


class SharedPreferenceHelperImpl
@Inject constructor(
    private val sharedPreference:SharedPreferences
) : SharedPreferenceHelper{

    companion object {
        val KEY_LAST_UPDATED_TIMESTAMP = "KEY_LAST_UPDATED_TIMESTAMP"
        val KEY_FILTER_STATUS = "KEY_FILTER_STATUS"
    }


    private val mSPE: SharedPreferences.Editor = sharedPreference.edit()
    private val mSP: SharedPreferences = sharedPreference



    override fun saveLastUpdatedTimestamp(long: Long) {
        mSPE.putLong(KEY_LAST_UPDATED_TIMESTAMP,long)
        mSPE.apply()
    }

    override fun getLastUpdatedTimestamp(): Long {
        return mSP.getLong(KEY_LAST_UPDATED_TIMESTAMP, Constants.DEFAULT_INT.toLong())
    }

    override fun saveFilterStatus(status: Boolean) {
        mSPE.putBoolean(KEY_FILTER_STATUS,status)
        mSPE.apply()
    }

    override fun getFilterStatus(): Boolean {
        return mSP.getBoolean(KEY_FILTER_STATUS, false)
    }


}