package ai.munim.bs23_android_task_101.data

import java.sql.Timestamp

interface SharedPreferenceHelper {

    companion object {
        const val MODULE_NAME = "ai.munim.bs23_android_task_101.PREFERENCE_FILE"
        const val VERSION = 1
    }


    fun saveLastUpdatedTimestamp(long: Long)

    fun getLastUpdatedTimestamp(): Long

    fun saveFilterStatus(boolean: Boolean)

    fun getFilterStatus(): Boolean

}