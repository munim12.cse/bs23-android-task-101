package ai.munim.bs23_android_task_101.data.impl

import ai.munim.bs23_android_task_101.data.DataManager
import ai.munim.bs23_android_task_101.data.DbHelper
import ai.munim.bs23_android_task_101.data.SharedPreferenceHelper
import ai.munim.bs23_android_task_101.dbhelper.entity.GithubRepoEntity
import androidx.paging.PagingSource
import javax.inject.Inject

class DataManagerImpl
@Inject constructor(
    private val dbHelper: DbHelper,
    private val preferenceHelper: SharedPreferenceHelper
    ): DataManager {


    override suspend fun saveGithubInfo(entity: GithubRepoEntity) {
        dbHelper.saveGithubInfo(entity)
    }

    override suspend fun saveGithubInfo(entity: ArrayList<GithubRepoEntity>) {
        dbHelper.saveGithubInfo(entity)
    }

    override suspend fun deleteGithubRepo(entity: GithubRepoEntity) {
        dbHelper.deleteGithubRepo(entity)
    }

    override suspend fun deleteAllGithubRepo() {
        dbHelper.deleteAllGithubRepo()
    }

    override suspend fun updateGithubRepo(entity: GithubRepoEntity) {
        dbHelper.updateGithubRepo(entity)
    }

    override suspend fun updateGithubRepo(entity: ArrayList<GithubRepoEntity>) {
        dbHelper.updateGithubRepo(entity)
    }

    override suspend fun getGithubRepoList(limit: Int, offset: Int): List<GithubRepoEntity> {
        return dbHelper.getGithubRepoList(limit,offset)
    }

    override suspend fun getSortedGithubRepoList(limit: Int, offset: Int): List<GithubRepoEntity>  {
        return dbHelper.getSortedGithubRepoList(limit, offset)
    }

    override fun getGithubRepoListTest(limit: Int, offset: Int): List<GithubRepoEntity> {
        TODO("Not yet implemented")
    }

    override fun getSortedGithubRepoListTest(limit: Int, offset: Int): List<GithubRepoEntity> {
        TODO("Not yet implemented")
    }

    override fun saveLastUpdatedTimestamp(long: Long) {
        preferenceHelper.saveLastUpdatedTimestamp(long)
    }

    override fun getLastUpdatedTimestamp(): Long {
        return preferenceHelper.getLastUpdatedTimestamp()
    }

    override fun saveFilterStatus(boolean: Boolean) {
        preferenceHelper.saveFilterStatus(boolean)
    }

    override fun getFilterStatus(): Boolean {
        return preferenceHelper.getFilterStatus()
    }


}