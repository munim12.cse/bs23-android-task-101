package ai.munim.bs23_android_task_101.data

import ai.munim.bs23_android_task_101.dbhelper.entity.GithubRepoEntity
import androidx.paging.PagingSource


interface DbHelper {

    suspend fun saveGithubInfo(entity: GithubRepoEntity)

    suspend fun saveGithubInfo(entity: ArrayList<GithubRepoEntity>)

    suspend fun deleteGithubRepo(entity: GithubRepoEntity)

    suspend fun deleteAllGithubRepo()

    suspend fun updateGithubRepo(entity: GithubRepoEntity)

    suspend fun updateGithubRepo(entity: ArrayList<GithubRepoEntity>)

    suspend fun getGithubRepoList(limit: Int, offset: Int) : List<GithubRepoEntity>

    suspend fun getSortedGithubRepoList(limit: Int, offset: Int) : List<GithubRepoEntity>

    fun getGithubRepoListTest(limit: Int, offset: Int) : List<GithubRepoEntity>

    fun getSortedGithubRepoListTest(limit: Int, offset: Int) : List<GithubRepoEntity>


}