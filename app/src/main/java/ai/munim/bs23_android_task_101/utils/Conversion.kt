package ai.munim.bs23_android_task_101.utils

import ai.munim.bs23_android_task_101.dbhelper.entity.GithubRepoEntity
import ai.munim.bs23_android_task_101.domain.model.githubRepoModel.GithubRepoModel

class Conversion {

    companion object {

        fun getGithubRepoEntityListFromGithubRepoModel(githubRepoModel: List<GithubRepoModel>): ArrayList<GithubRepoEntity>{
            val temp : ArrayList<GithubRepoEntity> = ArrayList()
            
            for (model in githubRepoModel){
                temp.add(
                    GithubRepoEntity(0,
                    model.id,
                    model.stargazersCount,
                    model.fullName,
                    model.name,
                    model.description,
                    model.createdAt,
                    model.watchers,
                    model.updatedAt,
                    model.stargazersUrl,
                    model.url,
                    model.watchersCount,
                    model.nodeId,
                    model.reposUrl,
                    model.avatarUrl,
                    model.login,
                    model.organizationsUrl
                ))
            }
            return temp
        }


    }

}