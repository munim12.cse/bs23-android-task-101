package ai.munim.bs23_android_task_101.utils

import ai.munim.bs23_android_task_101.BuildConfig
import android.content.Context
import android.net.ConnectivityManager
import android.view.WindowManager.BadTokenException
import android.widget.Toast
import java.text.SimpleDateFormat
import java.util.*

class CommonTasks {

    companion object {

        fun isOnline(context: Context?): Boolean {
            return if (context == null) {
                false
            } else {
                try {
                    val cm = context
                        .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    val netInfo = cm.activeNetworkInfo
                    if (netInfo != null && netInfo.isConnectedOrConnecting) {
                        return true
                    }
                    false
                } catch (e: Exception) {
                    false
                }
            }
        }


        fun showToast(context: Context?, message: String?) {
            try {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
            } catch (e: BadTokenException) {
                //use a log message
            }
        }


        fun getDateTimeFromIST(dateTime: String):String {

            val date = SimpleDateFormat(Constants.IST).parse(dateTime)
            val sdf = SimpleDateFormat(Constants.DateTimeFormat)
            sdf.timeZone = TimeZone.getTimeZone("GMT")

            val date_time_formate = sdf.format(date)
            return date_time_formate
        }


        fun isApiUpdated(lastUpdateTimestamp: Long):Boolean{
            if (lastUpdateTimestamp.equals(0)) return false;
            else {
                val currentTimeInMillis = System.currentTimeMillis()
                val api_updated_time_in_millis = lastUpdateTimestamp + Constants.LAST_UPDATED_THRESHOLD_IN_MILLIS
                if (api_updated_time_in_millis < currentTimeInMillis) return false;
                else return true;

            }

        }


        fun buildVariants(flavor: String): Boolean{
            return BuildConfig.BUILD_TYPE.equals(flavor)
        }

    }




}