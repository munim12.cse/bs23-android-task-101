package ai.munim.bs23_android_task_101.utils;


public interface Constants {

    String EMPTY_STRING = "";
    int DEFAULT_INT = 0;
    float DEFAULT_FLOAT = 0f;
    double DEFAULT_DOUBLE = 0.0;

    String Android = "Android";

    String DateTimeFormat = "MM-dd-yyyy HH:mm:ss";
    String IST = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    long LAST_UPDATED_THRESHOLD_IN_MILLIS = 30 * 60 * 1000;
    String developing = "developing";
    String production = "production";
}
