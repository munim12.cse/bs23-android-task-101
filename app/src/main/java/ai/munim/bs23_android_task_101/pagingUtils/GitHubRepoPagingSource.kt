package ai.munim.bs23_android_task_101.pagingUtils

import ai.munim.bs23_android_task_101.dbhelper.entity.GithubRepoEntity
import ai.munim.bs23_android_task_101.domain.repositories.HomeRepository
import ai.munim.bs23_android_task_101.network.interfaces.GithubRetrofitService
import ai.munim.bs23_android_task_101.network.model.githubDTOs.RepositoryDto
import ai.munim.bs23_android_task_101.network.model.githubDTOs.RepositoryDtoMapper
import ai.munim.bs23_android_task_101.utils.CommonTasks
import ai.munim.bs23_android_task_101.utils.Constants
import ai.munim.bs23_android_task_101.utils.Conversion
import androidx.paging.PagingSource
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import kotlinx.coroutines.delay
import retrofit2.HttpException

class GitHubRepoPagingSource (
   private val homeRepository: HomeRepository,
   private val githubRetrofitService: GithubRetrofitService,
   private val repositoryDtoMapper: RepositoryDtoMapper
        ): PagingSource<Int,GithubRepoEntity>() {

    override fun getRefreshKey(state: PagingState<Int, GithubRepoEntity>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, GithubRepoEntity> {

        return try {

            if (!CommonTasks.isApiUpdated(homeRepository.getLastUpdatedTimestamp())){
                val data = githubRetrofitService.getGithubRepoList(Constants.Android)
                homeRepository.saveGithubInfo(Conversion.getGithubRepoEntityListFromGithubRepoModel(repositoryDtoMapper.repoList(
                    data.items as List<RepositoryDto>
                )))
                homeRepository.saveLastUpdatedTimestamp(System.currentTimeMillis())
            }

            val page = params.key ?: 0
            val offset = page * params.loadSize

            if (homeRepository.getFilterStatus()){
                val entities = homeRepository.getSortedGithubRepoList(params.loadSize, offset)

                // simulate page loading
                if (page != 0) delay(1000)

                LoadResult.Page(
                    data = entities,
                    prevKey = if (page == 0) null else page - 1,
                    nextKey = if (entities.isEmpty()) null else page + 1
                )
            }else {
                val entities = homeRepository.getGithubRepoList(params.loadSize, offset)

                // simulate page loading
                if (page != 0) delay(1000)

                LoadResult.Page(
                    data = entities,
                    prevKey = if (page == 0) null else page - 1,
                    nextKey = if (entities.isEmpty()) null else page + 1
                )
            }

        } catch (e: Exception) {
            LoadResult.Error(e)
        } catch (e: HttpException) {
            return LoadResult.Error(e)
        }

    }
}