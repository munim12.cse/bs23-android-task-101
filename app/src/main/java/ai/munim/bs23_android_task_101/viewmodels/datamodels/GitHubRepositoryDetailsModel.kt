package ai.munim.bs23_android_task_101.viewmodels.datamodels

import ai.munim.bs23_android_task_101.utils.Constants
import android.os.Parcel
import android.os.Parcelable
import android.os.Parcelable.Creator


class GitHubRepositoryDetailsModel() : Parcelable {
    lateinit var description:String
    lateinit var owner : String
    lateinit var last_updated_date: String
    lateinit var avatarUrl: String

    constructor(parcel: Parcel) : this() {
        description = parcel.readString().toString()
        owner = parcel.readString().toString()
        last_updated_date = parcel.readString().toString()
        avatarUrl = parcel.readString().toString()
    }

    constructor(owner: String, description: String, last_updated_date: String,avatarUrl: String) : this() {
        this.owner = owner
        this.description = description
        this.last_updated_date = last_updated_date
        this.avatarUrl = avatarUrl

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(description)
        parcel.writeString(owner)
        parcel.writeString(last_updated_date)
        parcel.writeString(avatarUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Creator<GitHubRepositoryDetailsModel> {
        override fun createFromParcel(parcel: Parcel): GitHubRepositoryDetailsModel {
            return GitHubRepositoryDetailsModel(parcel)
        }

        override fun newArray(size: Int): Array<GitHubRepositoryDetailsModel?> {
            return arrayOfNulls(size)
        }
    }



}
