package ai.munim.bs23_android_task_101.viewmodels

import ai.munim.bs23_android_task_101.dbhelper.entity.GithubRepoEntity
import ai.munim.bs23_android_task_101.domain.repositories.HomeRepository
import androidx.lifecycle.*
import androidx.paging.cachedIn
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flatMapLatest

import javax.inject.Inject


@HiltViewModel
class HomeViewModel
    @Inject constructor(
        private val repository: HomeRepository,
    )
    : ViewModel(), LifecycleObserver{

    companion object {

    }

    init {

    }



    val flow = repository.githubRepository(10).cachedIn(viewModelScope)

    fun saveFilterStatus(boolean: Boolean) {
        repository.saveFilterStatus(boolean)
    }

    fun getFilterStatus(): Boolean {
        return repository.getFilterStatus()
    }




}
