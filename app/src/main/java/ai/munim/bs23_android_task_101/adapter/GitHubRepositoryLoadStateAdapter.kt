package ai.munim.bs23_android_task_101.adapter

import ai.munim.bs23_android_task_101.adapter.viewholders.NetworkStateItemViewHolder
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter

class GitHubRepositoryLoadStateAdapter(
    private val adapter: GitHubRepositoryListPagingAdapter
) : LoadStateAdapter<NetworkStateItemViewHolder>(){

    override fun onBindViewHolder(holder: NetworkStateItemViewHolder, loadState: LoadState) {
        holder.bindTo(loadState)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): NetworkStateItemViewHolder {
        return NetworkStateItemViewHolder(parent) { adapter.retry() }

    }
}