package ai.munim.bs23_android_task_101.adapter

import ai.munim.bs23_android_task_101.R
import ai.munim.bs23_android_task_101.adapter.viewholders.GitHubRepositoryViewHolder
import ai.munim.bs23_android_task_101.databinding.ItemViewerGithubRepositoryBinding
import ai.munim.bs23_android_task_101.dbhelper.entity.GithubRepoEntity
import ai.munim.bs23_android_task_101.domain.model.githubRepoModel.GithubRepoModel
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil

class GitHubRepositoryListPagingAdapter(diffUtils:DiffUtil.ItemCallback<GithubRepoEntity>)
    : PagingDataAdapter<GithubRepoEntity, GitHubRepositoryViewHolder>(diffUtils) {

    private lateinit var mOnItemClickListener: OnItemClickListener

    override fun onBindViewHolder(holder: GitHubRepositoryViewHolder, position: Int) {
        getItem(position)?.let { holder.bindTo(it,position) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GitHubRepositoryViewHolder {
        val binding: ItemViewerGithubRepositoryBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_viewer_github_repository, parent, false)

        return GitHubRepositoryViewHolder(binding,mOnItemClickListener)
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        mOnItemClickListener = onItemClickListener
    }

    interface OnItemClickListener {
        fun onItemClick(index: GithubRepoEntity, view: View)
    }


}