package ai.munim.bs23_android_task_101.adapter.viewholders

import ai.munim.bs23_android_task_101.R
import ai.munim.bs23_android_task_101.adapter.GitHubRepositoryListPagingAdapter
import ai.munim.bs23_android_task_101.databinding.FragmentGitHubRepositoryListBinding
import ai.munim.bs23_android_task_101.databinding.ItemViewerGithubRepositoryBinding
import ai.munim.bs23_android_task_101.dbhelper.entity.GithubRepoEntity
import ai.munim.bs23_android_task_101.domain.model.githubRepoModel.GithubRepoModel
import ai.munim.bs23_android_task_101.utils.CommonTasks
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class GitHubRepositoryViewHolder (
     private val mBinding: ItemViewerGithubRepositoryBinding,
     private val onItemClickListener: GitHubRepositoryListPagingAdapter.OnItemClickListener
        ) : RecyclerView.ViewHolder(mBinding.root), View.OnClickListener{

    init {
        itemView.setOnClickListener(this)

    }

    private lateinit var entity: GithubRepoEntity
    private var item_position: Int = 0

    fun bindTo(item: GithubRepoEntity, position: Int){
        entity = item
        item_position = position
        mBinding.txtFullName.setText(entity.fullName)
        mBinding.txtCreateDate.setText(String.format(itemView.context.getString(R.string.txt_created_at),entity.createdAt?.let { CommonTasks.getDateTimeFromIST(it) }))
        mBinding.txtStarCount.setText(String.format(itemView.context.getString(R.string.txt_stargazers_count),entity.stargazersCount?.toString()))

    }

    override fun onClick(p0: View?) {
        onItemClickListener.onItemClick(entity,p0!!)
    }
}