package ai.munim.bs23_android_task_101.adapter.diffutils

import ai.munim.bs23_android_task_101.dbhelper.entity.GithubRepoEntity
import android.text.TextUtils
import androidx.recyclerview.widget.DiffUtil
import org.w3c.dom.Text

class GitHubRepositoryItemCallBacks : DiffUtil.ItemCallback<GithubRepoEntity>() {

    override fun areItemsTheSame(oldItem: GithubRepoEntity, newItem: GithubRepoEntity): Boolean {
        return TextUtils.equals(oldItem.name, newItem.name)
    }

    override fun areContentsTheSame(oldItem: GithubRepoEntity, newItem: GithubRepoEntity): Boolean {
        return TextUtils.equals(oldItem.fullName,newItem.fullName)
    }

}