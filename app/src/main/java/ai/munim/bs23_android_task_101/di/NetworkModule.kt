package ai.munim.bs23_android_task_101.di

import ai.munim.bs23_android_task_101.network.interfaces.GithubRetrofitService
import ai.munim.bs23_android_task_101.network.model.githubDTOs.RepositoryDtoMapper
import ai.munim.bs23_android_task_101.utils.CommonTasks
import ai.munim.bs23_android_task_101.utils.Constants.production
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {


    @Singleton
    @Provides
    fun provideRepositoryMapper(): RepositoryDtoMapper {
        return RepositoryDtoMapper()
    }


    @Singleton
    @Provides
    fun provideOKHttpClient():OkHttpClient {
        return OkHttpClient.Builder()
            .build()
    }

    @Singleton
    @Provides
    fun provideOpenWeatherRetrofitService(okHttpClient: OkHttpClient): GithubRetrofitService {
        return Retrofit.Builder()
            .baseUrl(if (CommonTasks.buildVariants(production))
                "https://api.github.com/"
            else "https://api.github.com/"
        )
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
            .create(GithubRetrofitService::class.java)
    }



}