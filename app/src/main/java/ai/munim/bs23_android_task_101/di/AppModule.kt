package ai.munim.bs23_android_task_101.di

import ai.munim.bs23_android_task_101.BS23Task101App
import ai.munim.bs23_android_task_101.data.DataManager
import ai.munim.bs23_android_task_101.data.DbHelper
import ai.munim.bs23_android_task_101.data.SharedPreferenceHelper
import ai.munim.bs23_android_task_101.data.impl.DataManagerImpl
import ai.munim.bs23_android_task_101.data.impl.DbHelperImpl
import ai.munim.bs23_android_task_101.data.impl.SharedPreferenceHelperImpl
import ai.munim.bs23_android_task_101.dbhelper.DbConstants
import ai.munim.bs23_android_task_101.dbhelper.database.ProjectDB
import ai.munim.bs23_android_task_101.di.qualifiers.DatabaseInfo
import ai.munim.bs23_android_task_101.di.qualifiers.PreferenceInfo
import ai.munim.bs23_android_task_101.helper.PermissionManager
import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideApplication(@ApplicationContext app: Context): BS23Task101App {
        return app as BS23Task101App
    }


    @Provides
    @Singleton
    fun getPermissionManager(): PermissionManager {
        return PermissionManager()
    }



    @Provides
    @PreferenceInfo
    @Singleton
    fun provideSharedPreferenceName(): String = SharedPreferenceHelper.MODULE_NAME

    @Singleton
    @Provides
    fun getSharedPreference(
        @ApplicationContext context: Context,
        @PreferenceInfo moduleName: String?):
            SharedPreferences = context.getSharedPreferences(moduleName,Context.MODE_PRIVATE)

    @Provides
    @Singleton
    fun provideSharedPrefHelper(sharedPreferences: SharedPreferences):
            SharedPreferenceHelper {
        return SharedPreferenceHelperImpl(sharedPreferences)
    }

    @Provides
    @DatabaseInfo
    fun provideDatabaseName(): String {
        return DbConstants.DB_NAME
    }

    @Provides
    @Singleton
    fun provideAppDatabase(
        @DatabaseInfo dbName:String,
        @ApplicationContext context: Context
    ): ProjectDB{
        return Room.databaseBuilder(context,ProjectDB::class.java, dbName).build()
    }


    @Provides
    @Singleton
    fun provideDbHelper(projectDB: ProjectDB): DbHelper {
        return DbHelperImpl(projectDB)
    }


    @Provides
    @Singleton
    fun provideDataManager(dbHelper: DbHelper, preferenceHelper: SharedPreferenceHelper) :
            DataManager = DataManagerImpl(dbHelper, preferenceHelper)




}