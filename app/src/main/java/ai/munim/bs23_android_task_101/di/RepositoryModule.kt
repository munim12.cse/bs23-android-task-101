package ai.munim.bs23_android_task_101.di

import ai.munim.bs23_android_task_101.domain.repositories.HomeRepository
import ai.munim.bs23_android_task_101.domain.repositories.impl.HomeRepositoryImpl
import ai.munim.bs23_android_task_101.data.DataManager
import ai.munim.bs23_android_task_101.network.interfaces.GithubRetrofitService
import ai.munim.bs23_android_task_101.network.model.githubDTOs.RepositoryDtoMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideHomeRepository(
        githubRetrofitService: GithubRetrofitService,
        dataManager: DataManager,
        repositoryDtoMapper: RepositoryDtoMapper
    ): HomeRepository{
        return HomeRepositoryImpl(
            githubRetrofitService = githubRetrofitService,
            dataManager = dataManager,
            repositoryDtoMapper = repositoryDtoMapper
        )
    }



}