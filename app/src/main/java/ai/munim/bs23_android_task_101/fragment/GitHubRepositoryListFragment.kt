package ai.munim.bs23_android_task_101.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ai.munim.bs23_android_task_101.R
import ai.munim.bs23_android_task_101.adapter.GitHubRepositoryListPagingAdapter
import ai.munim.bs23_android_task_101.adapter.GitHubRepositoryLoadStateAdapter
import ai.munim.bs23_android_task_101.databinding.FragmentGitHubRepositoryListBinding
import ai.munim.bs23_android_task_101.dbhelper.entity.GithubRepoEntity
import ai.munim.bs23_android_task_101.utils.Constants
import ai.munim.bs23_android_task_101.viewmodels.HomeViewModel
import ai.munim.bs23_android_task_101.viewmodels.datamodels.GitHubRepositoryDetailsModel
import android.graphics.Color
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject


@AndroidEntryPoint
class GitHubRepositoryListFragment : Fragment() , GitHubRepositoryListPagingAdapter.OnItemClickListener,View.OnClickListener {
    
    private lateinit var mBinding : FragmentGitHubRepositoryListBinding
    private val mViewModel: HomeViewModel by viewModels()

    @Inject
    lateinit var adapter: GitHubRepositoryListPagingAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentGitHubRepositoryListBinding.inflate(inflater,container,false)
        return mBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initVariable()
        initListener()
        setupList()
    }



    fun initVariable(){

    }

    fun initListener(){
        adapter.setOnItemClickListener(this)
        mBinding.tvFilter.setOnClickListener(this)
        mBinding.swipeRefresh.setOnRefreshListener { adapter.refresh() }
    }

    private fun setupList() {
        mBinding.rvGithubRepository.adapter = adapter.withLoadStateFooter(
            footer = GitHubRepositoryLoadStateAdapter(adapter)
        )

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            adapter.loadStateFlow.collect { loadStates ->
                mBinding.swipeRefresh.isRefreshing = loadStates.refresh is LoadState.Loading
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            mViewModel.flow.collectLatest {
                adapter.submitData(it)
            }
        }

        mBinding.rvGithubRepository.layoutManager = LinearLayoutManager(context)
        val decoration = DividerItemDecoration(context, RecyclerView.VERTICAL)
        mBinding.rvGithubRepository.addItemDecoration(decoration)


    }

    override fun onResume() {
        super.onResume()

        if (mViewModel.getFilterStatus())
            mBinding.tvFilter.setBackgroundColor(Color.GREEN)

    }


    override fun onItemClick(index: GithubRepoEntity, view: View) {

        val direction = GitHubRepositoryListFragmentDirections.
        actionGitHubRepositoryListFragmentToGitHubRepositoryDetailsFragment(GitHubRepositoryDetailsModel(index.login?:Constants.EMPTY_STRING,
            index.description?:Constants.EMPTY_STRING,index.updatedAt?:Constants.EMPTY_STRING,index.avatarUrl?:Constants.EMPTY_STRING))
        findNavController().navigate(direction)

    }

    override fun onClick(p0: View?) {
        if (p0?.id == R.id.tv_filter && !mViewModel.getFilterStatus()){
            mViewModel.saveFilterStatus(true)
            mBinding.tvFilter.setBackgroundColor(Color.GREEN)
            adapter.refresh()
        }else if (p0?.id == R.id.tv_filter && mViewModel.getFilterStatus()){
            mViewModel.saveFilterStatus(false)
            mBinding.tvFilter.setBackgroundColor(Color.WHITE)
            adapter.refresh()
        }
    }


}