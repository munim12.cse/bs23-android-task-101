package ai.munim.bs23_android_task_101.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ai.munim.bs23_android_task_101.R
import ai.munim.bs23_android_task_101.databinding.FragmentGitHubRepositoryDetailsBinding
import ai.munim.bs23_android_task_101.utils.CommonTasks
import ai.munim.bs23_android_task_101.viewmodels.HomeViewModel
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint



@AndroidEntryPoint
class GitHubRepositoryDetailsFragment : Fragment() {


    private lateinit var mBinding : FragmentGitHubRepositoryDetailsBinding
    private val mViewModel: HomeViewModel by viewModels()
    private val args: GitHubRepositoryDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentGitHubRepositoryDetailsBinding.inflate(inflater,container,false)
        return mBinding.root
    }


    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initVariable()
        initListener()
        setView()

    }



    fun initVariable(){

    }

    fun initListener(){

    }

    private fun setView() {
        mBinding.tvDescription.setText(String.format(getString(R.string.txt_description),args.model.description))
        mBinding.tvOwner.setText(String.format(getString(R.string.txt_owner),args.model.owner))
        mBinding.tvLastUpdatedDate.setText(args.model.last_updated_date?.let {
            CommonTasks.getDateTimeFromIST(
                it
            )
        })


        context?.let {
            Glide.with(it)
                .load(args.model.avatarUrl)
                .placeholder(R.drawable.ic_user_placeholder)
                .error(R.drawable.ic_user_placeholder_error)
                .into(mBinding.imageView)
        }

    }



}