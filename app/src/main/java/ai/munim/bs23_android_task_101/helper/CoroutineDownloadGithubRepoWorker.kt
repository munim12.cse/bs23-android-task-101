package ai.munim.bs23_android_task_101.helper

import ai.munim.bs23_android_task_101.data.DataManager
import ai.munim.bs23_android_task_101.network.interfaces.GithubRetrofitService
import ai.munim.bs23_android_task_101.network.model.githubDTOs.RepositoryDto
import ai.munim.bs23_android_task_101.network.model.githubDTOs.RepositoryDtoMapper
import ai.munim.bs23_android_task_101.utils.Constants
import ai.munim.bs23_android_task_101.utils.Conversion
import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters

class CoroutineDownloadGithubRepoWorker constructor (private val githubRepoService: GithubRetrofitService,
                                                     private val dataManager: DataManager,
                                                     private val repositoryDtoMapper: RepositoryDtoMapper,
                                        appContext: Context,
                                        params: WorkerParameters) :
    CoroutineWorker(appContext, params) {

    override suspend fun doWork(): Result {
       val data = githubRepoService.getGithubRepoList(Constants.Android)

        dataManager.saveGithubInfo(Conversion.getGithubRepoEntityListFromGithubRepoModel(repositoryDtoMapper.repoList(
            data.items as List<RepositoryDto>
        )))

        return Result.success()
    }

}