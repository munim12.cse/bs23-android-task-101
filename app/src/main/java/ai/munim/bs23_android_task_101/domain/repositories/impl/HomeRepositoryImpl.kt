package ai.munim.bs23_android_task_101.domain.repositories.impl

import ai.munim.bs23_android_task_101.domain.repositories.HomeRepository
import ai.munim.bs23_android_task_101.data.DataManager
import ai.munim.bs23_android_task_101.dbhelper.entity.GithubRepoEntity
import ai.munim.bs23_android_task_101.pagingUtils.GitHubRepoPagingSource
import ai.munim.bs23_android_task_101.network.interfaces.GithubRetrofitService
import ai.munim.bs23_android_task_101.network.model.githubDTOs.RepositoryDtoMapper
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch


class HomeRepositoryImpl(
    private val githubRetrofitService: GithubRetrofitService,
    private val dataManager: DataManager,
    private val repositoryDtoMapper: RepositoryDtoMapper
) : HomeRepository {


    override fun githubRepository(limit: Int): Flow<PagingData<GithubRepoEntity>> = Pager(
        PagingConfig(
            pageSize = limit
        )
    ) {
        GitHubRepoPagingSource(this,githubRetrofitService,repositoryDtoMapper)
    }.flow

    override suspend fun saveGithubInfo(entity: GithubRepoEntity) {
        CoroutineScope(IO).launch {
            dataManager.saveGithubInfo(entity)
        }
    }

    override suspend fun saveGithubInfo(entity: ArrayList<GithubRepoEntity>) {
        CoroutineScope(IO).launch {
            dataManager.saveGithubInfo(entity)
        }
    }

    override suspend fun deleteGithubRepo(entity: GithubRepoEntity) {
        CoroutineScope(IO).launch {
            dataManager.saveGithubInfo(entity)
        }
        dataManager.deleteGithubRepo(entity)
    }

    override suspend fun deleteAllGithubRepo() {
        CoroutineScope(IO).launch {
            dataManager.deleteAllGithubRepo()
        }
    }

    override suspend fun updateGithubRepo(entity: GithubRepoEntity) {
        CoroutineScope(IO).launch {
            dataManager.updateGithubRepo(entity)
        }
    }

    override suspend fun updateGithubRepo(entity: ArrayList<GithubRepoEntity>) {
        CoroutineScope(IO).launch {
            dataManager.updateGithubRepo(entity)
        }
    }

    override suspend fun getGithubRepoList(limit: Int, offset: Int): List<GithubRepoEntity> {
        return dataManager.getGithubRepoList(limit, offset)
    }

    override suspend fun getSortedGithubRepoList(limit: Int, offset: Int): List<GithubRepoEntity> {
        return dataManager.getSortedGithubRepoList(limit,offset)
    }

    override fun saveLastUpdatedTimestamp(long: Long) {
        dataManager.saveLastUpdatedTimestamp(long)
    }

    override fun getLastUpdatedTimestamp(): Long {
        return dataManager.getLastUpdatedTimestamp()
    }

    override fun saveFilterStatus(boolean: Boolean) {
        dataManager.saveFilterStatus(boolean)
    }

    override fun getFilterStatus(): Boolean {
        return dataManager.getFilterStatus()
    }


}

