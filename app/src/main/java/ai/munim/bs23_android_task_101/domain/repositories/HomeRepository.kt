package ai.munim.bs23_android_task_101.domain.repositories

import ai.munim.bs23_android_task_101.dbhelper.entity.GithubRepoEntity
import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow

interface HomeRepository {

    fun githubRepository(limit: Int): Flow<PagingData<GithubRepoEntity>>

    suspend fun saveGithubInfo(entity: GithubRepoEntity)

    suspend fun saveGithubInfo(entity: ArrayList<GithubRepoEntity>)

    suspend fun deleteGithubRepo(entity: GithubRepoEntity)

    suspend fun deleteAllGithubRepo()

    suspend fun updateGithubRepo(entity: GithubRepoEntity)

    suspend fun updateGithubRepo(entity: ArrayList<GithubRepoEntity>)

    suspend fun getGithubRepoList(limit: Int, offset: Int) : List<GithubRepoEntity>

    suspend fun getSortedGithubRepoList(limit: Int, offset: Int) : List<GithubRepoEntity>

    fun saveLastUpdatedTimestamp(long: Long)

    fun getLastUpdatedTimestamp(): Long

    fun saveFilterStatus(boolean: Boolean)

    fun getFilterStatus(): Boolean


}