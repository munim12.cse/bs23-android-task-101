package ai.munim.bs23_android_task_101.domain.model.githubRepoModel

import com.google.gson.annotations.SerializedName

data class GithubRepoModel(

    val stargazersCount: Int? = null,

    val fullName: String? = null,

    val name: String? = null,

    val description: String? = null,

    val createdAt: String? = null,

    val watchers: Int? = null,

    val updatedAt: String? = null,

    val stargazersUrl: String? = null,

    val url: String? = null,

    val watchersCount: Int? = null,

    val nodeId: String? = null,

    val reposUrl: String? = null,

    val avatarUrl: String? = null,

    val login: String? = null,

    val id: Int? = null,

    val organizationsUrl: String? = null

)

