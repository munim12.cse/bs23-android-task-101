package ai.munim.bs23_android_task_101.data

import ai.munim.bs23_android_task_101.dbhelper.entity.GithubRepoEntity

object TestGithubRepositoryData {

    val gitHubRepoList = arrayOf(
        GithubRepoEntity(1,82128465,12878,"open-android",
            "Android","GitHub上最火的Android开源项目,所有开源项目都有详细资料和配套视频","2017-02-16T02:10:13Z",
            12878,"2022-11-24T04:45:11Z","https://api.github.com/repos/open-android/Android/stargazers",
            "https://api.github.com/repos/open-android/Android",12878,"MDEwOlJlcG9zaXRvcnk4MjEyODQ2NQ==","https://api.github.com/users/open-android/repos",
            "https://avatars.githubusercontent.com/u/23095877?v=4","open-android","https://api.github.com/users/open-android/orgs"
        ),GithubRepoEntity(2,12544093,3337,"hmkcode/Android",
            "Android","Android related examples","2013-09-02T16:12:28Z",
            3337,"2022-11-24T04:47:50Z","https://api.github.com/repos/hmkcode/Android/stargazers",
            "https://api.github.com/repos/hmkcode/Android",3337,"MDEwOlJlcG9zaXRvcnkxMjU0NDA5Mw==","https://api.github.com/users/hmkcode/repos",
            "https://avatars.githubusercontent.com/u/3790597?v=4","hmkcode","https://api.github.com/users/hmkcode/orgs"
        ),GithubRepoEntity(3,82128465,33100,"open-android/Android",
            "Android","GitHub上最火的Android开源项目","所有开源项目都有详细资料和配套视频",
            12878,"2022-11-24T04:47:50Z",
            "https://api.github.com/repos/hmkcode/Android/stargazers",
            "https://api.github.com/repos/hmkcode/Android",3337,
            "MDEwOlJlcG9zaXRvcnkxMjU0NDA5Mw==","https://api.github.com/users/hmkcode/repos",
            "https://avatars.githubusercontent.com/u/3790597?v=4","hmkcode",
            "https://api.github.com/users/hmkcode/orgs"
        )
    )
}