package ai.munim.bs23_android_task_101.espressouitest

import ai.munim.bs23_android_task_101.R
import ai.munim.bs23_android_task_101.activity.HomeActivity
import ai.munim.bs23_android_task_101.adapter.viewholders.GitHubRepositoryViewHolder
import ai.munim.bs23_android_task_101.data.DbHelper
import ai.munim.bs23_android_task_101.data.SharedPreferenceHelper.Companion.MODULE_NAME
import ai.munim.bs23_android_task_101.data.TestGithubRepositoryData
import ai.munim.bs23_android_task_101.data.impl.DbHelperImpl
import ai.munim.bs23_android_task_101.data.impl.SharedPreferenceHelperImpl.Companion.KEY_FILTER_STATUS
import ai.munim.bs23_android_task_101.dbhelper.DbConstants
import ai.munim.bs23_android_task_101.dbhelper.database.ProjectDB
import ai.munim.bs23_android_task_101.dbhelper.entity.GithubRepoEntity
import ai.munim.bs23_android_task_101.utils.EspressoIdlingResourceRule
import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import org.hamcrest.CoreMatchers.equalTo

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4ClassRunner::class)
class GitHubRepositoryDetailsFragmentTest {

    val LIST_ITEM_IN_TEST = 1

    lateinit var GITHUB_REPOSITORY_IN_TEST : GithubRepoEntity
    lateinit var context: Context
    lateinit var SP: SharedPreferences
    lateinit var SPE: SharedPreferences.Editor
    lateinit var dbHelper: DbHelper

    @Before
    fun init() {
        context = ApplicationProvider.getApplicationContext()
        SP = context.getSharedPreferences(MODULE_NAME,Context.MODE_PRIVATE)
        SPE = SP.edit()
        SPE.putBoolean(KEY_FILTER_STATUS,true)
        SPE.apply()
        dbHelper = DbHelperImpl(Room.databaseBuilder(context,ProjectDB::class.java, DbConstants.DB_NAME).build())
    }


    @get:Rule
    val activityRule = ActivityScenarioRule(HomeActivity::class.java)

    @get: Rule
    val espressoIdlingResourceRule = EspressoIdlingResourceRule()



    @Test
    fun a_filterStatusActive(){
        assertThat(SP.getBoolean(KEY_FILTER_STATUS,false), equalTo(true))
    }

    @Test
    fun b_selectFromSortedListItem_NavConfirmToDetailsFragment_DisplayDescription(){
        Espresso.onView(ViewMatchers.withId(R.id.rv_github_repository))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        if (SP.getBoolean(KEY_FILTER_STATUS,false)){
            val data_temp = dbHelper.getSortedGithubRepoListTest(10,0)
            GITHUB_REPOSITORY_IN_TEST = data_temp[LIST_ITEM_IN_TEST]
        }else {
            val data_temp = dbHelper.getGithubRepoListTest(10,0)
            GITHUB_REPOSITORY_IN_TEST = data_temp[LIST_ITEM_IN_TEST]
        }

        Espresso.onView(ViewMatchers.withId(R.id.rv_github_repository)).
        perform(
            RecyclerViewActions.actionOnItemAtPosition<GitHubRepositoryViewHolder>(
                LIST_ITEM_IN_TEST,
                ViewActions.click()
            )
        )


        // Confirm nav to DetailFragment and display description
        Espresso.onView(ViewMatchers.withId(R.id.tv_description))
            .check(ViewAssertions.matches(ViewMatchers.withText(String.format(context.getString(R.string.txt_description), GITHUB_REPOSITORY_IN_TEST.description))))
    }


}