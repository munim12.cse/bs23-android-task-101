package ai.munim.bs23_android_task_101.espressouitest

import ai.munim.bs23_android_task_101.activity.HomeActivity
import ai.munim.bs23_android_task_101.data.TestGithubRepositoryData
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.espresso.Espresso.onView
import ai.munim.bs23_android_task_101.R
import ai.munim.bs23_android_task_101.adapter.viewholders.GitHubRepositoryViewHolder
import ai.munim.bs23_android_task_101.utils.EspressoIdlingResourceRule
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.*
import org.junit.FixMethodOrder
import org.junit.runners.MethodSorters

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4ClassRunner::class)
class GitHubRepositoryListFragmentTest {


    val LIST_ITEM_IN_TEST = 1
    val GITHUB_REPOSITORY_IN_TEST  = TestGithubRepositoryData.gitHubRepoList[LIST_ITEM_IN_TEST]

    @get:Rule
    val activityRule = ActivityScenarioRule(HomeActivity::class.java)

    @get: Rule
    val espressoIdlingResourceRule = EspressoIdlingResourceRule()


    @Test
    fun listFragmentIsVisible_onAppLaunched(){
        onView(withId(R.id.rv_github_repository)).check(matches(isDisplayed()))

    }


    @Test
    fun backNavigation_toGithubRepoListFragment(){
        onView(withId(R.id.rv_github_repository)).
        perform(actionOnItemAtPosition<GitHubRepositoryViewHolder>(LIST_ITEM_IN_TEST,click()))

        Espresso.pressBack()

        // Confirm GithubRepoListFragment in view
        onView(withId(R.id.rv_github_repository)).check(matches(isDisplayed()))
    }



}