package ai.munim.bs23_android_task_101.espressouitest

import ai.munim.bs23_android_task_101.dbhelper.dao.GithubRepoDao
import ai.munim.bs23_android_task_101.dbhelper.database.ProjectDB
import ai.munim.bs23_android_task_101.dbhelper.entity.GithubRepoEntity
import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import org.hamcrest.CoreMatchers.equalTo

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException


@RunWith(AndroidJUnit4::class)
class GithubRepoEntityReadWriteTest {

    private lateinit var gitHubRepodao: GithubRepoDao
    private lateinit var db: ProjectDB

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, ProjectDB::class.java).build()
        gitHubRepodao = db.getGithubRepoDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeGithubRepoAndReadList() {
        val limit = 10
        val entityList: ArrayList<GithubRepoEntity> = ArrayList()
        for (i in 1..20){
            val entity = GithubRepoEntity(0,82128465,12878+(1..20).random(),"open-android",
                "Android","GitHub上最火的Android开源项目,所有开源项目都有详细资料和配套视频","2017-02-16T02:10:13Z",
                12878,"2022-11-24T04:45:11Z","https://api.github.com/repos/open-android/Android/stargazers",
                "https://api.github.com/repos/open-android/Android",12878,"MDEwOlJlcG9zaXRvcnk4MjEyODQ2NQ==","https://api.github.com/users/open-android/repos",
                "https://avatars.githubusercontent.com/u/23095877?v=4","open-android","https://api.github.com/users/open-android/orgs")
            entityList.add(entity)
        }

        gitHubRepodao.saveGithubInfoTest(entityList)
        val sortedList = gitHubRepodao.getPagedSortedGithubRepoListTest(10,0)
        assertThat(sortedList.size, equalTo(limit))

        var sorted_list_status =true
        var startCount = 0
        for (i in sortedList){
            if (startCount.equals(0))
                startCount = i.stargazersCount!!
            else if (startCount < i.stargazersCount!!){
                sorted_list_status = false
                break
            } else startCount = i.stargazersCount!!
        }

        assertThat(sorted_list_status, equalTo(true))

    }



}