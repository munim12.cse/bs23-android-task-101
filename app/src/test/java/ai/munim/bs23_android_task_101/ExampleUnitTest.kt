package ai.munim.bs23_android_task_101

import org.junit.Assert.*
import org.junit.Test
import java.io.FileInputStream
import java.io.IOException
import java.lang.Boolean.FALSE
import java.lang.Boolean.TRUE
import java.util.*


class ExampleUnitTest {


    @Test
    fun currentAppVersion() {
        val properties = Properties()
        try {
            FileInputStream("version.properties").use { fis ->
                properties.load(fis)
                val VERSION_MAJOR: String = properties.getProperty("VERSION_MAJOR")
                val VERSION_MINOR: String = properties.getProperty("VERSION_MINOR")
                val VERSION_PATCH: String = properties.getProperty("VERSION_PATCH")
                val VERSION_CODE: String = properties.getProperty("VERSION_CODE")
                val targetSdkVersion: String = properties.getProperty("targetSdkVersion")
                val expectation =
                    "VERSION_MAJOR:1,VERSION_MINOR:0,VERSION_PATCH:1,VERSION_CODE:1,targetSdkVersion:32"
                val found =
                    "VERSION_MAJOR:$VERSION_MAJOR,VERSION_MINOR:$VERSION_MINOR,VERSION_PATCH:$VERSION_PATCH,VERSION_CODE:$VERSION_CODE,targetSdkVersion:$targetSdkVersion"
                assertEquals(expectation, found)
            }
        } catch (ioex: IOException) {
            assertEquals(TRUE, FALSE)
        }
    }

}